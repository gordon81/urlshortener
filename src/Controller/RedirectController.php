<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\ShortUrl;
use App\Repository\ShortUrlRepository;
use App\Validator\Short;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RedirectController extends AbstractController
{

    private ShortUrlRepository $shortUrlRepository;

    public function __construct(ShortUrlRepository $shortUrlRepository)
    {
        $this->shortUrlRepository = $shortUrlRepository;
    }

    #[Route('/{short}', name: 'app_redirect', priority: -100)]
    public function index($short): Response
    {
        if (!Short::validateShort($short)) {
            return $this->json(['error' => 'Short is not Valide'], 422);
        }

        $shortUrl = $this->shortUrlRepository->findOneBy(['short' => $short]);

        if (!($shortUrl instanceof ShortUrl)) {
            return new Response('not found', 404);
        }

        $shortUrl->increaseHitByOne();
        $this->shortUrlRepository->save($shortUrl, true);

        return $this->redirect($shortUrl->getUrl());
    }


}
