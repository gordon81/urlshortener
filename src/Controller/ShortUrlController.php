<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\ShortUrlRepository;
use App\Service\ShortUrlGenerator;
use App\Validator\Short;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ShortUrlController extends AbstractController
{
    protected $shortUrlGenerator;
    private ShortUrlRepository $shortUrlRepository;

    public function __construct(ShortUrlGenerator $shortUrlGenerator, ShortUrlRepository $shortUrlRepository)
    {
        $this->shortUrlGenerator = $shortUrlGenerator;
        $this->shortUrlRepository = $shortUrlRepository;
    }

    #[Route('/shorturl', methods: ['POST'], name: 'app_short_url_create')]
    public function create(Request $request): JsonResponse
    {
        $url = json_decode($request->getContent())->url;
        if (!$this->validateUrl($url)) {
            return $this->json(['error' => ['message' => 'Url is not Valide']], 422);
        }
        $shortUrl = $this->shortUrlGenerator->addShort($url, true);
        return $this->json([
            'url' => $shortUrl->getUrl(),
            'shortUrl' => $this->generateUrl('app_redirect', ['short' => $shortUrl->getShort()], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);
    }

    #[Route('/shorturl', methods: ['GET', 'HEAD'], name: 'app_short_url_list')]
    public function index(): JsonResponse
    {
        $shortUrls = $this->shortUrlRepository->findAll();
        return $this->json($shortUrls);
    }

    #[Route('/shorturl/{short}', methods: ['GET', 'HEAD'], name: 'app_short_url_origin')]
    public function show(string $short): JsonResponse
    {
        if (!Short::validateShort($short)) {
            return $this->json(['error' => ['message' => 'Short is not Valide']], 422);
        }
        $shortUrl = $this->shortUrlRepository->findOneBy(['short' => $short]);
        return $this->json(['url' => $shortUrl->getUrl()]);

    }

    #[Route('/shorturl/{short}/hit', methods: ['GET', 'HEAD'], name: 'app_short_url_hit', priority: -90)]
    public function hit(string $short): JsonResponse
    {
        if (!Short::validateShort($short)) {
            return $this->json(['error' => ['message' => 'Short is not Valide']], 422);
        }
        $shortUrl = $this->shortUrlRepository->findOneBy(['short' => $short]);
        return $this->json(['hit' => $shortUrl->getHit()]);
    }

    public function validateUrl($url): bool
    {
        $path='';
        $urlSegnmente = parse_url($url);
        if(isset($urlSegnmente['path']) && $urlSegnmente['path'] !== null ){
            $encoded_path = array_map('urlencode', explode('/', $urlSegnmente['path']));
            $path =  implode('/', $encoded_path);
        }
        $domain = idn_to_ascii( $urlSegnmente['host']);
        $query = '';
        if(isset($urlSegnmente['query'])) {
            $querySegement = explode('&', $urlSegnmente['query']);
            foreach ($querySegement as &$queryPart) {
                $encoded_query = array_map('urlencode', explode('=', $queryPart));
                $queryPart = implode('=', $encoded_query);
            }
            $query = '?'.implode('&',$querySegement);
        }
        $asciiUrl = $urlSegnmente['scheme'].'://'.$domain.$path.$query;
        return filter_var($asciiUrl, FILTER_VALIDATE_URL) ? true : false;
    }
}
