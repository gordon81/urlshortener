<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\ShortUrlRepository;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ShortUrlRepository::class)]
class ShortUrl implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Assert\Url(
        message: 'The url {{ value }} is not a valid url',
    )]
    #[ORM\Column(length: 255)]
    private string $url = '';

    #[ORM\Column(nullable: true)]
    private ?int $hit = 0;

    #[ORM\Column(length: 255)]
    private ?string $short = null;

    static public function do($data)
    {
        $short = new self();
        $short->setUrl($data['url']);
        $short->setShort($data['short']);
        $short->setHit($data['hit'] ?? 0);
        return $short;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHit(): ?int
    {
        return $this->hit;
    }

    public function setHit(?int $hit): self
    {
        $this->hit = $hit;

        return $this;
    }

    public function increaseHitByOne()
    {
        $this->hit++;
    }

    public function jsonSerialize(): mixed
    {
        return ['short' => $this->getShort(), 'url' => $this->getUrl()];
    }

    public function getShort(): ?string
    {
        return $this->short;
    }

    public function setShort(string $short): self
    {
        $this->short = $short;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
