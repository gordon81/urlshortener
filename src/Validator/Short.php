<?php

namespace App\Validator;

class Short
{

    static public function validateShort(string $short):bool
    {
        $cleanShort = preg_replace('/[^a-zA-Z0-9]/', '', $short);
        if (strlen($short) === 6 && strlen($cleanShort) === 6) {
            return true;
        }
        return false;
    }
}