<?php

namespace App\DataFixtures;

use App\Entity\ShortUrl;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ShortUrlFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $fakeShortUrlData = [
            ['url' => 'https://www.ommax-digital.com/de/', 'hit' => 10, 'short' => '123456'],
            ['url' => 'https://typo3.org', 'hit' => 1, 'short' => 'abcdef'],
            ['url' => 'https://psg-der-freien-voltigierer.de/', 'hit' => 2, 'short' => 'a1b2c3'],
            ['url' => 'https://google.de', 'hit' => 0, 'short' => 'google'],
            ['url' => 'https://symfony.com/', 'hit' => 5, 'short' => '5gfp19'],
            ['url' => 'https://symfony.com/bundles/DoctrineFixturesBundle/current/index.html', 'hit' => 0, 'short' => 'fdf4f0'],
            ['url' => 'https://phpunit.readthedocs.io/en/9.5/index.html', 'hit' => 2, 'short' => '9d7ddd'],
            ['url' => 'https://reactjs.org/', 'hit' => 0, 'short' => '979a16']
        ];

        foreach ($fakeShortUrlData as $modelData) {
            $manager->persist(ShortUrl::do($modelData));
        }

        $manager->flush();
    }
}
