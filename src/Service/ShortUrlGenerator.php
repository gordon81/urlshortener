<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Short;
use App\Entity\ShortUrl;
use App\Repository\ShortUrlRepository;
use Psr\Log\LoggerInterface;


class ShortUrlGenerator
{
    private $logger;

    private ShortUrlRepository $shortUrlRepository;

    public function __construct(LoggerInterface $logger, ShortUrlRepository $shortUrlRepository)
    {
        $this->logger = $logger;
        $this->shortUrlRepository = $shortUrlRepository;
    }

    public function addShort($url): ShortUrl
    {
        $shortUrl = $this->shortUrlRepository->findOneBy(['url' => $url]);
        if ($shortUrl instanceof ShortUrl) {
            return $shortUrl;
        }

        $shortUrl = ShortUrl::do(['url' => $url, 'short' => $this->generateShort()]);
        $this->shortUrlRepository->save($shortUrl, true);

        return $shortUrl;
    }

    /**
     * @return string
     */
    protected function generateShort(): string
    {
        $short = substr(md5(microtime()), rand(0, 26), 6);
        $shortUrl = $this->shortUrlRepository->findOneBy(['short' => $short]);
        if ($shortUrl instanceof ShortUrl) {
            $short = $this->generateShort();
        }
        return $short;
    }

}