Projekt Start

Wenn ddev vorhanden oder Installieren https://ddev.readthedocs.io/en/stable/ dann


*   ddev start
*   ddev ssh
*   php bin/console doctrine:database:create
*   php bin/console doctrine:schema:create

Beispiel Daten
*   php bin/console doctrine:fixtures:load

api -> https://urlshortener.ddev.site/

frontend react -> https://urlshortener.ddev.site/frontend

------------------------------------------------
Kein ddev dann local mit Apache oder Nginx Webroot ist der public Ordner

*   composer install
*   php bin/console doctrine:database:create
*   php bin/console doctrine:schema:create

Beispiel Daten
*   php bin/console doctrine:fixtures:load



Für das Frontend in den frontend Ordner gehen und

*   npm install && npm run-script start

Nginx Configuration für das Frontend 

    location /frontend {
        proxy_pass  http://localhost:3000;
    }

Apache : (ungetestet)

    <Location "/frontend/">
        ProxyPass "http://localhost:3000"
    </Location>


