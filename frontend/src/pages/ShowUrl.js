import React from "react";
import ShowUrlByShortUrl from "../components/ShowUrlByShortUrl";

export default class ShowUrl extends React.Component {
    render() {
        return (<div>
            <h1>Show Original Url </h1>
            <ShowUrlByShortUrl/>
        </div>);
    }
}