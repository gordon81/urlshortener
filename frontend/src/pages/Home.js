import React from "react";
import NewShortUrl from "../components/NewShortUrl";

export default class Home extends React.Component {
    render() {
        return (
            <div>
                <h1>Url Shortener</h1>
                <div>
                    <NewShortUrl/>
                </div>
            </div>
        );
    }
}