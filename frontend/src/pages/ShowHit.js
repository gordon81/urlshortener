import React from "react";
import ShowHitOfShortUrl from "../components/ShowHitOfShortUrl";

export default class ShowHit extends React.Component {
    render() {
        return (<div>
            <h1>Show hit of redirect </h1>
            <ShowHitOfShortUrl/>
        </div>);
    }
}