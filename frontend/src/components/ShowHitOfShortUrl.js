import React from "react";
import {endPointHit} from "../config";
import BasisForm from "./Form/BasisForm";
import MyButton from "./Form/MyButton";

export default class ShowHitOfShortUrl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            result: ''
        };
        this.changeValue = this.changeValue.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    changeValue(value) {
        fetch(endPointHit.replace(/#\w+#/g, value))
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        result: result,
                        error: result.error ? result.error : null
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    handleClick() {
        this.setState({
            error: null,
            isLoaded: false
        });
    }

    render() {
        const {error, isLoaded, result} = this.state;
        if (error) {
            return (<div>
                <div>Error: {error.message} </div>
                <div><MyButton buttonText={'reset'} handleClick={this.handleClick}/></div>
            </div>);
        } else if (!isLoaded) {
            return <div><BasisForm label={"Short"} changeValue={this.changeValue}/></div>;
        } else {
            return (<div>
                <div>{result.hit}</div>
                <MyButton buttonText={'show hit from another'} handleClick={this.handleClick}/>
            </div>);
        }
    }


}