import React from "react";
import PropTypes from "prop-types";

class MyButton extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.handleClick();
    }

    render() {
        return (
            <button onClick={this.handleClick}>
                {this.props.buttonText}
            </button>
        );
    }
}

MyButton.propTypes = {
    buttonText: PropTypes.string,
    handleClick: PropTypes.func,
};
MyButton.defaultProps = {
    buttonText: '',
    handleClick: () => {
    }
};
export default MyButton;