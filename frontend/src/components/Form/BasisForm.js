import React from "react";
import PropTypes from 'prop-types';


class BasisForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        this.props.changeValue(this.state.value)
        event.preventDefault(event);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    {this.props.label}:
                    <input type="text" value={this.state.value} onChange={this.handleChange}/>
                </label>
                <input type="submit" value="Submit"/>
            </form>
        );
    }
}

BasisForm.propTypes = {
    label: PropTypes.string,
    changeValue: PropTypes.func,
};
BasisForm.defaultProps = {
    label: ''
};

export default BasisForm;