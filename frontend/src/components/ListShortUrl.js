import React from "react";

import {domain, endPointList} from "../config"

export default class ListShortUrl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch(endPointList)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result,
                        error: result.error ? result.error : null
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }


    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <table>
                    <thead>
                    <tr>
                        <td>Short Url</td>
                        <td>Origin Url</td>
                    </tr>
                    </thead>
                    <tbody>
                    {items.map(item => (
                        <tr key={item.short}>
                            <td>
                                <a href={domain + item.short} target={"_blank"} rel="noreferrer">{domain + item.short}</a>
                            </td>
                            <td>
                                <a href={item.url} target={"_blank"} rel="noreferrer">{item.url}</a>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            );
        }
    }

}

