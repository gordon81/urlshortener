const domain = 'https://urlshortener.ddev.site/';

const endPointList = 'https://urlshortener.ddev.site/shorturl';

const endPointHit = 'https://urlshortener.ddev.site/shorturl/#short#/hit'

const endPointOriginalUrl = 'https://urlshortener.ddev.site/shorturl/';

const endPointCreate = 'https://urlshortener.ddev.site/shorturl';



export {
    domain, endPointList, endPointHit, endPointOriginalUrl, endPointCreate
};
