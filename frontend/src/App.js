import './App.css';
import React from "react";
import {BrowserRouter, Link, Route, Routes} from "react-router-dom";
import ListPage from "./pages/ListPage"
import ShowHit from "./pages/ShowHit";
import ShowUrl from "./pages/ShowUrl";
import Home from "./pages/Home";

function App() {
    return (
        <div>
            <BrowserRouter basename='/frontend/'>

                <nav className={"mainMenu"}>
                    <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/list">List</Link>
                        </li>
                        <li>
                            <Link to="/show">Show</Link>
                        </li>
                        <li>
                            <Link to="/showhit">Show Hit</Link>
                        </li>
                    </ul>
                </nav>

                <div className={"content"}>
                    <Routes>
                        <Route path="/showhit" element={<ShowHit/>}/>
                        <Route path="/show" element={<ShowUrl/>}/>
                        <Route path="/list" element={<ListPage/>}/>
                        <Route path="/" element={<Home/>}/>
                    </Routes>
                </div>

            </BrowserRouter>
        </div>
    );
}

export default App;
