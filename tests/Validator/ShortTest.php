<?php

namespace App\Tests\Validator;

use App\Validator\Short;
use PHPUnit\Framework\TestCase;

class ShortTest extends TestCase
{

    public function testValidateShort()
    {
        $this->assertEquals(
            true,
            Short::validateShort('abc1AD')
        );
    }

    public function testValidateShortToLong()
    {
        $this->assertEquals(
            false,
            Short::validateShort('abc1ADfg')
        );
    }

    public function testValidateShortWrongCharater()
    {
        $this->assertEquals(
            false,
            Short::validateShort('a1!bcs')
        );
    }
}
